#ifndef UNIVERSAL_SHADOW_CASTER_PASS_INCLUDED
#define UNIVERSAL_SHADOW_CASTER_PASS_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"

float3 _LightDirection;

struct Attributes
{
    float4 positionOS   : POSITION;
    float3 normalOS     : NORMAL;
    float2 texcoord     : TEXCOORD0;
    UNITY_VERTEX_INPUT_GPUANIMATION
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
    float2 uv           : TEXCOORD0;
    float4 positionCS   : SV_POSITION;
};

UNITY_INSTANCING_BUFFER_START(Props)
    UNITY_DEFINE_INSTANCED_PROP(float3, textureCoordinates)
    UNITY_DEFINE_INSTANCED_PROP(float4x4, objectToWorlds)
UNITY_INSTANCING_BUFFER_END(Props)

float4 GetShadowPositionHClip(Attributes input)
{
    float3 coordinate = UNITY_ACCESS_INSTANCED_PROP(Props, textureCoordinates);
    float4x4 objectToWorld = UNITY_ACCESS_INSTANCED_PROP(Props, objectToWorlds);

    float3 positionWS = TransformObjectToWorld_GPUAnimation_Shadow(input.positionOS.xyz, input.boneIds, input.boneInfluences, coordinate, objectToWorld);
    
    float3 normalWS = TransformObjectToWorldNormal_GPUAnimation_Shadow(input.normalOS, input.boneIds, input.boneInfluences, coordinate, objectToWorld);

    float4 positionCS = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, _LightDirection));

#if UNITY_REVERSED_Z
    positionCS.z = min(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
#else
    positionCS.z = max(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
#endif

    return positionCS;
}

Varyings ShadowPassVertex(Attributes input)
{
    Varyings output;
    UNITY_SETUP_INSTANCE_ID(input);

    output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
    output.positionCS = GetShadowPositionHClip(input);
    return output;
}

half4 ShadowPassFragment(Varyings input) : SV_TARGET
{
    Alpha(SampleAlbedoAlpha(input.uv, TEXTURE2D_ARGS(_BaseMap, sampler_BaseMap)).a, _BaseColor, _Cutoff);
    return 0;
}

#endif
