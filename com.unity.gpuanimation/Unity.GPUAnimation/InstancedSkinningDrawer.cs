using System;
using System.Linq;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

namespace Unity.GPUAnimation
{
    public class InstancedSkinningDrawer : IDisposable
    {
        private Material material;

        private Mesh mesh;

        public unsafe InstancedSkinningDrawer(Material srcMaterial, Mesh meshToDraw, AnimationTextures animTexture)
        {
            this.mesh = meshToDraw;
            this.material = new Material(srcMaterial);

            this.material.SetTexture("_AnimationTexture0", animTexture.Animation0);
            this.material.SetTexture("_AnimationTexture1", animTexture.Animation1);
            this.material.SetTexture("_AnimationTexture2", animTexture.Animation2);

            _textureCoordinatesBufferID = Shader.PropertyToID("textureCoordinates");
            _objectToWorldBufferID = Shader.PropertyToID("objectToWorlds");
        }

        public void Dispose()
        {
            UnityEngine.Object.DestroyImmediate(material);
        }

        private MaterialPropertyBlock m_Block = new MaterialPropertyBlock();
        private readonly int _textureCoordinatesBufferID;
        private readonly int _objectToWorldBufferID;

        private static Vector4[] m_Coordinates = new Vector4[1000];
        private static Matrix4x4[] m_Matrices = new Matrix4x4[1000];
        public void Draw(NativeArray<Vector4> TextureCoordinates, NativeArray<Matrix4x4> ObjectToWorld, ShadowCastingMode shadowCastingMode, bool receiveShadows) {
            // CHECK: Systems seem to be called when exiting playmode once things start getting destroyed, such as the mesh here.
            if (mesh == null || material == null)
                return;

            int count = TextureCoordinates.Length;
            if (count == 0)
                return;

            int offset = 0;
            int times = Mathf.FloorToInt(count / m_Coordinates.Length);
            for (int t = 0; t < times; t++) {
                Draw(TextureCoordinates, ObjectToWorld, shadowCastingMode, receiveShadows, offset, m_Coordinates.Length);
                offset += m_Coordinates.Length;
            }
            Draw(TextureCoordinates, ObjectToWorld, shadowCastingMode, receiveShadows, offset, count - offset);
        }

        private void Draw(NativeArray<Vector4> TextureCoordinates, NativeArray<Matrix4x4> ObjectToWorld,
                ShadowCastingMode shadowCastingMode, bool receiveShadows,
                int offset, int max) {
            for (int i = 0; i < max; i++) {
                m_Coordinates[i] = TextureCoordinates[offset + i];
                m_Matrices[i] = ObjectToWorld[offset + i];
            }

            m_Block.SetVectorArray(_textureCoordinatesBufferID, m_Coordinates);
            m_Block.SetMatrixArray(_objectToWorldBufferID, m_Matrices);
            Graphics.DrawMeshInstanced(mesh, 0, material, m_Matrices, max, m_Block, shadowCastingMode, receiveShadows);
        }
    }
}
