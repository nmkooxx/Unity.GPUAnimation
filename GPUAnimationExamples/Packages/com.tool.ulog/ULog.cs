using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using ULogFrame;
using System.Threading;
using System.Diagnostics;
using Object = UnityEngine.Object;
using Debug = UnityEngine.Debug;

public class ULog : MonoBehaviour, ILogHandler {
    private class LogInfo {
        public LogType type;

        public string condition;

        public string stackTrace;

        public string[] tags;

        public bool HaveTag(string tag) {
            if (tags == null) {
                return false;
            }
            for (int i = 0; i < tags.Length; i++) {
                if (tags[i] == tag) {
                    return true;
                }
            }
            return false;
        }

    }

    private class TagInfo {
        public string tag;
        public bool active;
    }

    private class TextInfo {
        public GameObject gameObject;

        public RectTransform rectTransform;

        public Text text;

        public LayoutElement layout;
    }

    private static ULog s_instance;

    private ULogCanvas m_ui;

    private GameObject m_panelFull;

    private GameObject m_goOpen;

    private static List<LogInfo> m_cacheLogs = new List<LogInfo>();

    private static List<LogInfo> m_showLogs = new List<LogInfo>();

    private static List<string> m_writeLogs = new List<string>();

    private static string m_logPath;

    private StreamWriter m_writer;

    private static Dictionary<string, StreamWriter> m_writers = new Dictionary<string, StreamWriter>();

    public bool writeLogLog;

    public bool writeWarningLog = true;

    public bool showLogLog = true;

    public bool showWarningLog = true;

    public bool showErrorLog = true;

    public bool showStackTrace = true;

    public bool tagDefaultActive = true;

    public int LogNumMax = 2000;

    public int LogNumInPage = 10;

    private int m_pageIndex;

    private int m_pageNum;

    private Queue<TextInfo> m_pool = new Queue<TextInfo>(10);

    private List<TextInfo> m_shows = new List<TextInfo>(10);

    private Dictionary<string, TagInfo> m_tagInfos = new Dictionary<string, TagInfo>();

    public static ULog Create(Font font = null) {
        if (s_instance != null) {
            return s_instance;
        }
        if (font != null) {
            ULogCanvas.SetFont(font);
        }
        GameObject gameObject = new GameObject("LogCanvas", new Type[]
        {
            typeof(RectTransform)
        });
        ULog uLog = gameObject.AddComponent<ULog>();
        s_instance.m_rawHandler = Debug.unityLogger.logHandler;
        Debug.unityLogger.logHandler = s_instance;
        uLog.Hide();
        return uLog;
    }

    public static void SetFont(Font font) {
        ULogCanvas.SetFont(font);
    }

    public static void AddUpdate(Action action) {
        if (s_instance == null) {
            Debug.LogError("ULogView not Create!!");
            return ;
        }
        s_instance.AddUpdateEv(action);
    }

    public static Button AddButton(string name, UnityAction action) {
        if (s_instance == null) {
            Debug.LogError("ULogView not Create!!");
            return null;
        }
        return s_instance.AddButtonEv(name, action);
    }

    public static Toggle AddToggle(string name, UnityAction<bool> action, bool isOn) {
        if (s_instance == null) {
            Debug.LogError("ULogView not Create!!");
            return null;
        }
        return s_instance.AddToggleEv(name, action, isOn, s_instance.m_ui.Scroll_Func.content);
    }

    public static void SetInputCommond(Func<string, string> handler) {
        if (s_instance == null) {
            Debug.LogError("ULogView not Create!!");
            return;
        }
        s_instance.SetInputFieldEv(handler);
    }

    public static void Append(string name, string log) {
        StreamWriter writer = GetWriter(name);
        writer.WriteLine(log);
        writer.Flush();
    }

    private void Awake() {
        if (s_instance != null) {
            Debug.LogError("ULogView reduplicate times");
        }
        s_instance = this;
        //Object.DontDestroyOnLoad(base.gameObject);
        m_ui = new ULogCanvas(base.gameObject);
        m_panelFull = m_ui.Panel_Full.gameObject;
        m_goOpen = m_ui.btn_Open.gameObject;
        m_ui.btn_Open.onClick.AddListener(OnClickShowFull);
        m_ui.btn_Mini.onClick.AddListener(OnClickShowMini);
        m_ui.btn_Clear.onClick.AddListener(OnClickClear);
        m_ui.btn_Close.onClick.AddListener(OnClickClose);
        m_ui.btn_Prev.onClick.AddListener(OnClickPrevPage);
        m_ui.btn_Next.onClick.AddListener(OnClickNextPage);
        m_ui.btn_PrevMore.onClick.AddListener(OnClickPrevMorePage);
        m_ui.btn_NextMore.onClick.AddListener(OnClickNextMorePage);
        m_ui.toggle_Setting.onValueChanged.AddListener(OnToggleSettingChanged);
        m_ui.toggle_Log.onValueChanged.AddListener(OnToggleLogChanged);
        m_ui.toggle_Func.onValueChanged.AddListener(OnToggleFuncChanged);
        m_ui.toggle_Input.onValueChanged.AddListener(OnToggleInputChanged);

        m_ui.Input_Commond.onEndEdit.AddListener(OnEndEdit);

        AddToggleEv("showLogLog", OnToggleShowLogChanged, showLogLog, m_ui.SettingContent);
        AddToggleEv("showWarningLog", OnToggleShowWarningChanged, showWarningLog, m_ui.SettingContent);
        AddToggleEv("showErrorLog", OnToggleShowErrorChanged, showErrorLog, m_ui.SettingContent);
        AddToggleEv("showStackTrace", OnToggleShowStackTraceChanged, showStackTrace, m_ui.SettingContent);
        AddToggleEv("tagDefaultActive", OnToggleDefaultTagChanged, tagDefaultActive, m_ui.SettingContent);

        CreateLogWriter();
        if (m_showFull) {
            OnClickShowFull();
        }
        else {
            OnClickShowMini();
            RefreshPageIndex();
       }
    }

    private void Update() {
        WriteLogs();

        UpdateFPS();

        Recognize();

        UpdateEvents();
    }

    private void OnDestroy() {
        DestroyLogWriter();
    }

    /// <summary>
    /// 缓存数值字符串
    /// </summary>
    private static string[] s_fpsStrs = new string[1000];
    private static string FPSToString(float fps) {
        int index = (int)(fps * 10);
        if (index >= s_fpsStrs.Length) {
            index = s_fpsStrs.Length - 1;
        }
        string str = s_fpsStrs[index];
        if (str == null) {
            str = fps.ToString("0.0");
            s_fpsStrs[index] = str;
        }
        return str;
    }

    private const float kSampleDura = 0.5f;
    private float m_fpsTime = 0;
    private int m_fpsFrame = 0;

    private void UpdateFPS() {
        if (m_ui == null || m_ui.txt_FPS == null) {
            return;
        }

        m_fpsFrame += 1;
        m_fpsTime += Time.unscaledDeltaTime;

        // 刷新帧率
        if (m_fpsTime >= kSampleDura) {
            float fps = m_fpsFrame / m_fpsTime;
            m_ui.txt_FPS.text = FPSToString(fps);
            m_fpsFrame = 0;
            m_fpsTime = 0;
        }
    }

    public bool isShow { get; private set; }
    public void Show() {
        isShow = true;
        m_ui.btn_Open.gameObject.SetActive(true);
        m_ui.Panel_Full.gameObject.SetActive(false);
    }

    public void Hide() {
        isShow = false;
        m_ui.btn_Open.gameObject.SetActive(false);
        m_ui.Panel_Full.gameObject.SetActive(false);
    }

    #region Gesture Recognizer
    public bool enableRecognize = true;
    public float stratRecognizePressTime = 1f;
    public enum eDirection {
        Up,
        Down,
        Left,
        Right
    }

    public eDirection[] recognizeDirections = new eDirection[] {
        eDirection.Up,
        eDirection.Up,
        eDirection.Down,
        eDirection.Down,
        eDirection.Left,
        eDirection.Right,
        eDirection.Left,
        eDirection.Right,
    };


    private bool m_startRecognize;
    private int m_recognizeIndex;
    private Vector3 m_lastMousePosition;
    private float m_lastMouseDownTime;

    private void Recognize() {
        if (!enableRecognize || isShow) {
            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            m_lastMousePosition = Input.mousePosition;
            m_lastMouseDownTime = Time.time;
        }

        if (Input.GetMouseButtonUp(0)) {
            if (!m_startRecognize) {
                m_startRecognize = Time.time > m_lastMouseDownTime + stratRecognizePressTime;
                m_recognizeIndex = 0;
//                 if (m_startRecognize) {
//                     Debug.Log("Recognize Start");
//                 }
            } else {
                eDirection direction;
                float deltaX = Input.mousePosition.x - m_lastMousePosition.x;
                float deltaY = Input.mousePosition.y - m_lastMousePosition.y;
                if (Mathf.Abs(deltaX) < Mathf.Abs(deltaY)) {
                    if (deltaY > 0) {
                        direction = eDirection.Up;
                    } else {
                        direction = eDirection.Down;
                    }
                } else {
                    if (deltaX > 0) {
                        direction = eDirection.Right;
                    } else {
                        direction = eDirection.Left;
                    }
                }
                //Debug.Log("Recognize:" + direction + " " + Input.mousePosition + " " + m_lastMousePosition + " delta:" + deltaX + "," + deltaY);

                if (direction == recognizeDirections[m_recognizeIndex]) {
                    m_recognizeIndex += 1;
                    if (m_recognizeIndex >= recognizeDirections.Length) {
                        m_startRecognize = false;
                        m_recognizeIndex = 0;
                        Show();
                    }
                } else {
                    m_startRecognize = false;
                    m_recognizeIndex = 0;
                }
            }
        }
    }

    #endregion Gesture Recognizer

    #region Log
    private void CreateLogWriter() {
        Application.logMessageReceived += OnHandleLog;
        DateTime now = DateTime.Now;
        m_logPath = Application.temporaryCachePath + "/Log/";
        if (!Directory.Exists(m_logPath)) {
            Directory.CreateDirectory(m_logPath);
        }
        string filePath = m_logPath + now.DayOfWeek.ToString() + ".txt";
        FileInfo fileInfo = new FileInfo(filePath);
        if (fileInfo.Exists) {
            DateTime lastWriteTime = File.GetLastWriteTime(filePath);
            if (lastWriteTime.Year != now.Year || lastWriteTime.Month != now.Month || lastWriteTime.Day != now.Day) {
                m_writer = fileInfo.CreateText();
            } else {
                try {
                    m_writer = fileInfo.AppendText();
                } catch (IOException ex) {
                    Debug.LogError(filePath + "\n" + ex);
                } finally {
                    m_writer = null;
                }
            }
        } else {
            m_writer = fileInfo.CreateText();
        }
        string dateInfo = now.ToString("yyyyMMdd_HH:mm:ss");
        string startLog = string.Format("{0} {1} {2}\n{3}\n", new object[]
        {
            "-------LogCatch.Start-------",
            dateInfo,
            Application.version,
            filePath
        });
        m_writeLogs.Add(startLog);
        AddLogCache(startLog, string.Empty, LogType.Assert, null);
    }

    private static StreamWriter GetWriter(string name) {
        StreamWriter streamWriter;
        if (m_writers.TryGetValue(name, out streamWriter)) {
            return streamWriter;
        }
        if (string.IsNullOrEmpty(m_logPath)) {
            m_logPath = Application.temporaryCachePath + "/Log/";
            if (!Directory.Exists(m_logPath)) {
                Directory.CreateDirectory(m_logPath);
            }
        }
        string path = m_logPath + name + ".txt";
        FileInfo fileInfo = new FileInfo(path);
        if (fileInfo.Exists) {
            streamWriter = fileInfo.AppendText();
        } else {
            streamWriter = fileInfo.CreateText();
        }
        streamWriter.WriteLine("\nTime:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        streamWriter.Flush();
        m_writers[name] = streamWriter;

        //Debug.Log("GetWriter:" + path);
        return streamWriter;
    }

    public void Log(string condition, string stackTrace, LogType type) {
        OnHandleLog(condition, stackTrace, type);
    }

    public void DestroyLogWriter() {
        if (m_writer != null) {
            m_writer.Write("-------LogCatch.End---------\n\n");
            m_writer.Flush();
            m_writer.Close();
            m_writer.Dispose();
            m_writer = null;
        }
        foreach (var item in m_writers.Values) {
            item.Close();
            item.Dispose();
        }
        ULog.m_writers.Clear();
    }

    private bool CanWrite(LogType type) {
        bool result = true;
        switch (type) {
            case LogType.Warning:
                result = writeWarningLog;
                break;
            case LogType.Log:
                result = writeLogLog;
                break;
        }
        return result;
    }

    private bool CanShow(LogType type) {
        bool result = true;
        switch (type) {
            case LogType.Error:
                result = showErrorLog;
                break;
            case LogType.Warning:
                result = showWarningLog;
                break;
            case LogType.Log:
                result = showLogLog;
                break;
        }
        return result;
    }

    private const int kTagMax = 20;
    private List<string> m_tmpTags = new List<string>(3);
    private List<string> FindTags(string condition) {
        m_tmpTags.Clear();
        int startIndex = 0;
        if (condition[startIndex] != '[') {
            return m_tmpTags;
        }
        int len = 0;
        for (int index = 1; index < condition.Length; index++) {
            ++len;
            if (len > kTagMax) {
                //标签不能超过20个字符
                break;
            }
            if (condition[index] != ']') {
                continue;
            }
            string tag = condition.Substring(startIndex + 1, len-1);
            m_tmpTags.Add(tag);
            len = 0;
            startIndex = index + 1;
            if (condition[startIndex] != '[') {
                //不存在下一个标签则跳出
                break;
            }
            index += 1;
        }
        return m_tmpTags;
    }

    private TagInfo GetTagInfo(string tag) {
        TagInfo info;
        if (!m_tagInfos.TryGetValue(tag, out info)) {
            info = new TagInfo();
            info.tag = tag;
            info.active = tagDefaultActive;
            m_tagInfos[tag] = info;

            AddToggleEv(tag, (enable)=> { OnToggleShowTagChanged(enable, tag); }, info.active, m_ui.SettingContent);
        }
        return info;
    }

    private void OnHandleLog(string condition, string stackTrace, LogType type) {
        string dataStr = null;
        if (CanWrite(type)) {
            dataStr = DateTime.Now.ToString("HH:mm:ss.fff");
            string text = string.Format("{0} [{1}]\n{2}\n{3}\n",
                dataStr,
                type,
                condition,
                stackTrace
            );
            m_writeLogs.Add(text);
        }
        if (Application.isPlaying) {
            if (string.IsNullOrEmpty(dataStr)) {
                dataStr = DateTime.Now.ToString("HH:mm:ss.fff");
            }
            string text = string.Format("{0} [{1}]\n{2}\n",
                dataStr,
                type,
                condition
            );
            AddLogCache(text, stackTrace, type, FindTags(condition));
        }
    }

    private string[] kEmptyTag = new string[] { "None" };
    private void AddLogCache(string condition, string stackTrace, LogType type, List<string> tags) {
        if (m_cacheLogs.Count > LogNumMax) {
            m_cacheLogs.RemoveAt(0);
        }
        LogInfo logInfo = new LogInfo();
        logInfo.condition = condition;
        logInfo.stackTrace = stackTrace;
        logInfo.type = type;
        if (tags == null || tags.Count == 0) {
            logInfo.tags = kEmptyTag;
        } else {
            logInfo.tags = tags.ToArray();
        }
        m_cacheLogs.Add(logInfo);
        bool canShow = CanShow(type);
        for (int i = 0; i < logInfo.tags.Length; i++) {
            string tag = logInfo.tags[i];
            TagInfo tagInfo = GetTagInfo(tag);
            if (canShow && tagInfo.active) {
                //避免多个标签重复插入
                canShow = false;
                if (m_showLogs.Count > LogNumMax) {
                    m_showLogs.RemoveAt(0);
                }
                m_showLogs.Add(logInfo);
            }
        }
        int num = m_cacheLogs.Count / LogNumInPage + 1;
        if (m_pageNum != num) {
            m_pageNum = num;
            RefreshPageIndex();
        }
    }

    private void WriteLogs() {
        if (m_writeLogs.Count <= 0 || m_writer == null) {
            return;
        }

        for (int i = 0; i < m_writeLogs.Count; i++) {
            m_writer.Write(m_writeLogs[i]);
        }
        m_writer.Flush();
        m_writeLogs.Clear();
    }
    #endregion Log

    #region UI
    private void FilterLog() {
        m_showLogs.Clear();
        for (int index = 0; index < m_cacheLogs.Count; index++) {
            LogInfo logInfo = m_cacheLogs[index];
            bool canShow = CanShow(logInfo.type);
            if (!canShow) {
                continue;
            }
            for (int i = 0; i < logInfo.tags.Length; i++) {
                string tag = logInfo.tags[i];
                TagInfo tagInfo = GetTagInfo(tag);
                if (tagInfo.active) {
                    m_showLogs.Add(logInfo);
                    break;
                }
            }
        }

        //重新显示
        m_pageIndex = -1;
        RefreshPage(0);
    }

    private void RefreshPageIndex() {
        if (m_ui.num == null) {
            return;
        }
        m_ui.num.text = (m_pageIndex + 1) + "\n" + m_pageNum;
    }

    private bool m_showFull = false;
    public void OnClickShowFull() {
        //Debug.Log("OnClickShowFull");
        m_showFull = true;
        m_goOpen.SetActive(false);
        m_panelFull.SetActive(true);
        int pageIndex = m_pageIndex;
        m_pageIndex = -1;
        RefreshPage(pageIndex);
    }

    public void OnClickShowMini() {
        //Debug.Log("OnClickShowMini");
        m_showFull = false;
        m_goOpen.SetActive(true);
        m_panelFull.SetActive(false);
    }

    private void PopText(LogInfo log) {
        TextInfo textInfo;
        if (m_pool.Count > 0) {
            textInfo = m_pool.Dequeue();
        } else {
            textInfo = new TextInfo();
            textInfo.text = Instantiate<Text>(m_ui.txt_Log);
            textInfo.gameObject = textInfo.text.gameObject;
            textInfo.rectTransform = (textInfo.text.transform as RectTransform);
            textInfo.rectTransform.SetParent(m_ui.Scroll_Log.content);
            textInfo.rectTransform.localScale = Vector3.one;
            textInfo.layout = textInfo.gameObject.GetComponent<LayoutElement>();
        }
        textInfo.gameObject.SetActive(true);
        textInfo.rectTransform.SetAsLastSibling();
        if (showStackTrace) {
            textInfo.text.text = log.condition + log.stackTrace;
        } else {
            textInfo.text.text = log.condition;
        }
        switch (log.type) {
            case LogType.Error:
                textInfo.text.color = Color.red;
                break;
            case LogType.Assert:
                textInfo.text.color = Color.green;
                break;
            case LogType.Warning:
                textInfo.text.color = Color.yellow;
                break;
            case LogType.Log:
                textInfo.text.color = Color.white;
                break;
            case LogType.Exception:
                textInfo.text.color = Color.red;
                break;
        }
        int num = (int)textInfo.text.preferredHeight + 5;
        textInfo.layout.minHeight = num;
        m_shows.Add(textInfo);
    }

    private void PushText(TextInfo cell) {
        cell.gameObject.SetActive(false);
        m_pool.Enqueue(cell);
    }

    private void ClearText() {
        int count = m_shows.Count;
        for (int i = 0; i < count; i++) {
            PushText(m_shows[i]);
        }
        m_shows.Clear();
    }

    private void RefreshPage(int pageIndex) {
        if (pageIndex < 0 || pageIndex >= m_pageNum) {
            return;
        }
        if (m_pageIndex == pageIndex) {
            return;
        }
        m_pageIndex = pageIndex;
        ClearText();
        int startIdx = pageIndex * LogNumInPage;
        int endIdx = startIdx + LogNumInPage;
        if (endIdx >= m_showLogs.Count) {
            endIdx = m_showLogs.Count;
        }
        for (int i = startIdx; i < endIdx; i++) {
            PopText(m_showLogs[i]);
        }
        RefreshPageIndex();
    }

    private void OnClickPrevPage() {
        RefreshPage(m_pageIndex - 1);
    }

    private void OnClickPrevMorePage() {
        int pageIndex = m_pageIndex - 10;
        pageIndex = pageIndex > 0 ? pageIndex : 0;
        RefreshPage(pageIndex);
    }

    private void OnClickNextPage() {
        RefreshPage(m_pageIndex + 1);
    }

    private void OnClickNextMorePage() {
        int pageIndex = m_pageIndex + 10;
        pageIndex = pageIndex < m_pageNum ? pageIndex : m_pageNum - 1;
        RefreshPage(pageIndex);
    }

    private void OnClickClear() {
        ClearText();
        m_showLogs.Clear();
        var frist = m_cacheLogs[0];
        m_cacheLogs.Clear();
        m_cacheLogs.Add(frist);
        m_pageIndex = 0;
        m_pageNum = 0;
        RefreshPageIndex();
    }

    private void OnClickClose() {
        Hide();
        //m_ui.go.SetActive(false);
    }

    private bool m_settingChanged = false;
    private void OnToggleSettingChanged(bool enable) {
        m_ui.SettingContent.gameObject.SetActive(enable);
        m_ui.LogContent.gameObject.SetActive(!enable);
        if (enable) {
            m_ui.Scroll_Log.content = m_ui.SettingContent;
        } else {
            m_ui.Scroll_Log.content = m_ui.LogContent;
            if (m_settingChanged) {
                m_settingChanged = false;
                FilterLog();
            }
        }
    }

    private void OnToggleLogChanged(bool enable) {
        m_ui.Scroll_Log.gameObject.SetActive(enable);
        if (enable) {
            int pageIndex = m_pageIndex;
            m_pageIndex = -1;
            RefreshPage(pageIndex);
        }
    }

    private void OnToggleFuncChanged(bool enable) {
        m_ui.Scroll_Func.gameObject.SetActive(enable);
    }

    private void OnToggleInputChanged(bool enable) {
        m_ui.Scroll_Input.gameObject.SetActive(enable);
        if (enable) {
            m_ui.go.SetActive(false);
            m_ui.go.SetActive(true);
        }
    }

    private void OnToggleShowLogChanged(bool enable) {
        m_settingChanged = true;

        showLogLog = enable;
    }

    private void OnToggleShowWarningChanged(bool enable) {
        m_settingChanged = true;

        showWarningLog = enable;
    }

    private void OnToggleShowErrorChanged(bool enable) {
        m_settingChanged = true;

        showErrorLog = enable;
    }

    private void OnToggleShowStackTraceChanged(bool enable) {
        m_settingChanged = true;

        showStackTrace = enable;
    }

    private void OnToggleDefaultTagChanged(bool enable) {
        tagDefaultActive = enable;
    }

    private void OnToggleShowTagChanged(bool enable, string tag) {
        m_settingChanged = true;

        TagInfo tagInfo = GetTagInfo(tag);
        tagInfo.active = enable;
    }

    private void OnClickInputTextCell(Text text) {
        m_ui.Input_Commond.text = text.text;
    }

    private const int kMaxCommond = 10;
    private Queue<Text> m_commonds = new Queue<Text>();
    private void OnEndEdit(string value) {
        string result;
        if (m_inputCommondHandler == null) {
            result = "Commond hasn't Init!";
        } else {
            result = m_inputCommondHandler(value);
        }

        Text textInfo;
        if (m_pool.Count >= kMaxCommond) {
            textInfo = m_commonds.Dequeue();
        } else {
            textInfo = Instantiate<Text>(m_ui.txt_Input, m_ui.Scroll_Input.content);
            textInfo.gameObject.SetActive(true);
        }
        textInfo.rectTransform.SetAsLastSibling();
        textInfo.text = result;

        m_ui.Input_Commond.text = string.Empty;
    }
    #endregion UI

    #region
    private ILogHandler m_rawHandler;

    public static bool EnableShowStrace = true;

    /// <summary>
    /// 完善日志信息，如：线程、时间
    /// </summary>
    /// <param name="log">原始日志</param>
    /// <param name="showStrace">是否显示堆栈，（注意：同时受堆栈总开关影响）</param>
    /// <returns>处理好的日志</returns>
    private static string MakeLog(string log, bool showStrace) {
        string threadId = Thread.CurrentThread.ManagedThreadId.ToString();
        string strDateTime = DateTime.Now.ToString("MM-dd HH:mm:ss ffff");
        log = log + "\n[" + strDateTime + "]" + "[thread:" + threadId + "]";
        if (showStrace && EnableShowStrace) {
            string stackInfo = new StackTrace(true).ToString();
            log = log + "StackTrace:\n" + stackInfo + "\n";
        } else {
            log += "\n";
        }

        return log;
    }

    public void LogException(Exception exception, Object context) {
        m_rawHandler.LogException(exception, context);
    }

    public void LogFormat(LogType logType, Object context, string format, params object[] args) {
        string log = MakeLog(format, false);
        m_rawHandler.LogFormat(logType, context, log, args);
    }

#if UNITY_EDITOR
    [UnityEditor.Callbacks.OnOpenAssetAttribute(0)]
    private static bool OnOpenAsset(int instanceId, int line) {
        string stackTrace = GetStackTrace();
        if (string.IsNullOrEmpty(stackTrace) || !stackTrace.Contains("ULog:Log")) {
            return false;
        }

        System.Text.RegularExpressions.Match matches = System.Text.RegularExpressions.Regex.Match(stackTrace, @"\(at (.+)\)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        string pathLine = "";
        while (matches.Success) {
            pathLine = matches.Groups[1].Value;

            if (!pathLine.Contains("ULog.cs")) {
                int splitIndex = pathLine.LastIndexOf(":");
                string path = pathLine.Substring(0, splitIndex);
                line = System.Convert.ToInt32(pathLine.Substring(splitIndex + 1));
                string fullPath = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("Assets"));
                fullPath = fullPath + path;
                UnityEditorInternal.InternalEditorUtility.OpenFileAtLineExternal(fullPath.Replace('/', '\\'), line);
                break;
            }
            matches = matches.NextMatch();
        }
        return true;
    }

    private static Type ConsoleWindowType = typeof(UnityEditor.EditorWindow).Assembly.GetType("UnityEditor.ConsoleWindow");
    private static System.Reflection.FieldInfo ConsoleWindowFieldInfo = ConsoleWindowType.GetField("ms_ConsoleWindow",
        System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
    private static System.Reflection.FieldInfo ActiveTextFieldInfo = ConsoleWindowType.GetField("m_ActiveText",
        System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
    private static string GetStackTrace() {
        var consoleInstance = ConsoleWindowFieldInfo.GetValue(null);
        if (consoleInstance == null) {
            return null;
        }

        if ((object)UnityEditor.EditorWindow.focusedWindow != consoleInstance) {
            return null;
        }

        string activeText = ActiveTextFieldInfo.GetValue(consoleInstance).ToString();
        return activeText;
    }
#endif
    #endregion

    private List<Action> m_updateEvents;
    public void AddUpdateEv(Action action) {
        if (m_updateEvents == null) {
            m_updateEvents = new List<Action>();
        }
        m_updateEvents.Add(action);
    }

    private void UpdateEvents() {
        if (m_updateEvents == null) {
            return;
        }
        for (int i = 0; i < m_updateEvents.Count; i++) {
            if (m_updateEvents[i] != null) {
                try {
                    m_updateEvents[i]();
                } catch (Exception) {
                    throw;
                }
            }
        }
    }

    public Button AddButtonEv(string name, UnityAction action) {
        Button button = GameObject.Instantiate<Button>(m_ui.btn_Item);
        button.gameObject.SetActive(true);
        button.transform.SetParent(m_ui.Scroll_Func.content);
        button.onClick.AddListener(action);
        Text componentInChildren = button.GetComponentInChildren<Text>();
        componentInChildren.text = name;
        return button;
    }

    public Toggle AddToggleEv(string name, UnityAction<bool> action, bool isOn, RectTransform parent) {
        Toggle toggle = GameObject.Instantiate<Toggle>(m_ui.toggle_Item);
        toggle.gameObject.SetActive(true);
        toggle.transform.SetParent(parent);
        toggle.isOn = isOn;
        toggle.onValueChanged.AddListener(action);
        Text componentInChildren = toggle.GetComponentInChildren<Text>();
        componentInChildren.text = name;
        return toggle;
    }

    private Func<string, string> m_inputCommondHandler;
    public void SetInputFieldEv(Func<string, string> handler) {
        m_inputCommondHandler = handler;
    }
}
