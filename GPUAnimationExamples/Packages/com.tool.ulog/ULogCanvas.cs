using System;
using UnityEngine;
using UnityEngine.UI;

namespace ULogFrame {
    internal class ULogCanvas {
        private static Font m_font;

        public static void SetFont(Font font) {
            m_font = font;
        }

        public object data { get; set; }

        public GameObject go { get; protected set; }

        public RectTransform rectXform { get; protected set; }

        public RectTransform Panel_Full { get; protected set; }

        public Button btn_Mini { get; protected set; }

        public Text Text { get; protected set; }

        public Button btn_Close { get; protected set; }

        public ScrollRect Scroll_Log { get; protected set; }

        public RectTransform LogContent { get; protected set; }

        public RectTransform SettingContent { get; protected set; }

        public Text txt_Log { get; protected set; }

        public ScrollRect Scroll_Func { get; protected set; }

        public Button btn_Item { get; protected set; }

        public Toggle toggle_Item { get; protected set; }

        public Image Bottom { get; protected set; }

        public Text num { get; protected set; }

        public Toggle toggle_Log { get; protected set; }

        public Toggle toggle_Func { get; protected set; }

        public Toggle toggle_Input { get; protected set; }

        public Button btn_Prev { get; protected set; }

        public Button btn_Next { get; protected set; }

        public Button btn_PrevMore { get; protected set; }

        public Button btn_NextMore { get; protected set; }

        public Button btn_Clear { get; protected set; }

        public Toggle toggle_Setting { get; protected set; }

        public RectTransform Panel_Mini { get; protected set; }

        public Button btn_Open { get; protected set; }

        public Text txt_FPS { get; protected set; }

        public ScrollRect Scroll_Input { get; protected set; }
        public Text txt_Input { get; protected set; }
        public InputField Input_Commond { get; protected set; }

        public ULogCanvas() {
            go = new GameObject("LogCanvas", new Type[]
            {
				typeof(RectTransform)
            });
        }

        public ULogCanvas(GameObject go) {
            Create(go);
        }

        public void Create(GameObject root) {
            if (root == null) {
                Debug.LogError(this + " Create root null");
                return;
            }

            go = root;
            Canvas canvas = go.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            //永远排在最上层
            canvas.sortingOrder = 9999;

            GameObject.DontDestroyOnLoad(go);

            CanvasScaler otherCanvasScaler = GameObject.FindObjectOfType<CanvasScaler>();
            CanvasScaler canvasScaler = go.AddComponent<CanvasScaler>();
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            if (otherCanvasScaler != null && canvasScaler.uiScaleMode == otherCanvasScaler.uiScaleMode) {
                canvasScaler.referenceResolution = otherCanvasScaler.referenceResolution;
                canvasScaler.screenMatchMode = otherCanvasScaler.screenMatchMode;
                canvasScaler.matchWidthOrHeight = otherCanvasScaler.matchWidthOrHeight;

                //go.transform.localScale = otherCanvasScaler.transform.localScale;
            } else {
                //默认iPhone6为标准分辨率
                canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
                //默认iPhone6为标准分辨率
                if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
                    //竖屏
                    canvasScaler.referenceResolution = new Vector2(750f, 1334f);
                    canvasScaler.matchWidthOrHeight = 0f;
                }
                else {
                    canvasScaler.referenceResolution = new Vector2(1334f, 750f);
                    canvasScaler.matchWidthOrHeight = 1f;
                }
            }
            go.AddComponent<GraphicRaycaster>();
            rectXform = (go.transform as RectTransform);
            RectTransform panelFullXform = CreateRectTransform("Panel_Full", rectXform, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.one, Vector2.one * 0.5f);
            Panel_Full = panelFullXform;
            RectTransform miniXform = CreateRectTransform("btn_Mini", panelFullXform, new Vector2(-120f, 0f), new Vector2(100f, 70f), Vector2.one, Vector2.one, Vector2.one);
            btn_Mini = CreateButton(miniXform, "_", 50);
            RectTransform closeXform = CreateRectTransform("btn_Close", panelFullXform, new Vector2(0f, 0f), new Vector2(100f, 70f), Vector2.one, Vector2.one, Vector2.one);
            btn_Close = CreateButton(closeXform, "X", 50);

            ToggleGroup toggleGroup = panelFullXform.gameObject.AddComponent<ToggleGroup>();
            RectTransform toggleLogXform = CreateRectTransform("toggle_log", panelFullXform, new Vector2(0f, 0f), new Vector2(100f, 70f), new Vector2(0f, 1f), new Vector2(0f, 1f), new Vector2(0f, 1f));
            toggle_Log = CreateToggle(toggleLogXform, "Log", 40);
            toggle_Log.group = toggleGroup;
            toggle_Log.isOn = true;

            RectTransform toggleFuncXform = CreateRectTransform("toggle_func", panelFullXform, new Vector2(110f, 0f), new Vector2(100f, 70f), new Vector2(0f, 1f), new Vector2(0f, 1f), new Vector2(0f, 1f));
            toggle_Func = CreateToggle(toggleFuncXform, "Func", 40);
            toggle_Func.group = toggleGroup;

            RectTransform toggleInputXform = CreateRectTransform("toggle_input", panelFullXform, new Vector2(220f, 0f), new Vector2(100f, 70f), new Vector2(0f, 1f), new Vector2(0f, 1f), new Vector2(0f, 1f));
            toggle_Input = CreateToggle(toggleInputXform, "Input", 36);
            toggle_Input.group = toggleGroup;

            RectTransform scrollLogXform = CreateRectTransform("Scroll_Log", panelFullXform, new Vector2(0f, -70f), new Vector2(0f, -150f), Vector2.zero, Vector2.one, new Vector2(0.5f, 1f));
            Scroll_Log = CreateScrollRect(scrollLogXform);
            //Scroll_Log.horizontal = false;
            LogContent = Scroll_Log.content;
            Scroll_Log.content.sizeDelta = new Vector2(2500f, 100f);
            VerticalLayoutGroup verticalLayoutGroup = Scroll_Log.content.gameObject.AddComponent<VerticalLayoutGroup>();
            verticalLayoutGroup.childForceExpandHeight = false;
            verticalLayoutGroup.childForceExpandWidth = false;
            verticalLayoutGroup.spacing = 20f;
            ContentSizeFitter contentSizeFitter = Scroll_Log.content.gameObject.AddComponent<ContentSizeFitter>();
            contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.MinSize;
            RectTransform txtLogXform = CreateRectTransform("txt_Log", Scroll_Log.content, new Vector2(0f, 0f), new Vector2(Scroll_Log.content.rect.width, 10f), Vector2.up, Vector2.up, new Vector2(0f, 1f));
            LayoutElement layoutElement = txtLogXform.gameObject.AddComponent<LayoutElement>();
            layoutElement.minWidth = Scroll_Log.content.rect.width;
            layoutElement.minHeight = 10f;
            txt_Log = CreateText(txtLogXform, string.Empty, 28);
            txt_Log.gameObject.SetActive(false);

            RectTransform content2Xform = CreateRectTransform("Content2", Scroll_Log.viewport);
            content2Xform.anchorMin = Vector2.up;
            content2Xform.anchorMax = Vector2.one;
            content2Xform.sizeDelta = new Vector2(0, 300);
            content2Xform.pivot = Vector2.up;
            SettingContent = content2Xform;

            GridLayoutGroup gridLayoutGroup = SettingContent.gameObject.AddComponent<GridLayoutGroup>();
            gridLayoutGroup.cellSize = new Vector2(238f, 100f);
            gridLayoutGroup.spacing = new Vector2(2, 2);
            contentSizeFitter = SettingContent.gameObject.AddComponent<ContentSizeFitter>();
            contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            content2Xform.gameObject.SetActive(false);

            RectTransform bottomXform = CreateRectTransform("Bottom", scrollLogXform, new Vector2(0f, -80f), new Vector2(0f, 80f), Vector2.zero, new Vector2(1f, 0f), new Vector2(0.5f, 0f));
            Bottom = CreateImage(bottomXform, new Color32(81, 177, 255, 255));
            RectTransform clearXform = CreateRectTransform("btn_Clear", bottomXform, new Vector2(0f, 0f), new Vector2(100f, 80f), new Vector2(0f, 0f), new Vector2(0f, 0f), new Vector2(0f, 0f));
            btn_Clear = CreateButton(clearXform, "Clear", 40);
            RectTransform toggleSettingXform = CreateRectTransform("toggle_Setting", bottomXform, new Vector2(110f, 0f), new Vector2(100f, 80f), new Vector2(0f, 0f), new Vector2(0f, 0f), new Vector2(0f, 0f));
            toggle_Setting = CreateToggle(toggleSettingXform, "setting", 30);
            RectTransform xform7 = CreateRectTransform("btn_PrevMore", bottomXform, new Vector2(-430f, 0f), new Vector2(100f, 80f), new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(1f, 0f));
            btn_PrevMore = CreateButton(xform7, "<<", 50);
            RectTransform btn_PrevXform = CreateRectTransform("btn_Prev", bottomXform, new Vector2(-320f, 0f), new Vector2(100f, 80f), new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(1f, 0f));
            btn_Prev = CreateButton(btn_PrevXform, "<", 50);
            RectTransform txtNumXform = CreateRectTransform("num", bottomXform, new Vector2(-200f, 0f), new Vector2(150f, 80f), new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(1f, 0f));
            num = CreateText(txtNumXform, "???\n???", 28);
            num.color = Color.black;
            num.alignment = TextAnchor.MiddleCenter;
            RectTransform btn_NextXform = CreateRectTransform("btn_Next", bottomXform, new Vector2(-110f, 0f), new Vector2(100f, 80f), new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(1f, 0f));
            btn_Next = CreateButton(btn_NextXform, ">", 50);
            RectTransform xform10 = CreateRectTransform("btn_NextMore", bottomXform, new Vector2(0f, 0f), new Vector2(100f, 80f), new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(1f, 0f));
            btn_NextMore = CreateButton(xform10, ">>", 50);

            RectTransform scrollButton = CreateRectTransform("Scroll_Button", panelFullXform, new Vector2(0f, -70f), new Vector2(0f, -70f),
                Vector2.zero, Vector2.one, new Vector2(0.5f, 1f));
            Scroll_Func = CreateScrollRect(scrollButton);
            Scroll_Func.horizontal = false;
            Scroll_Func.gameObject.SetActive(false);
            gridLayoutGroup = Scroll_Func.content.gameObject.AddComponent<GridLayoutGroup>();
            gridLayoutGroup.cellSize = new Vector2(238f, 100f);
            gridLayoutGroup.spacing = new Vector2(2, 2);
            contentSizeFitter = Scroll_Func.content.gameObject.AddComponent<ContentSizeFitter>();
            contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            RectTransform buttonItemXform = CreateRectTransform("btn_Item", Scroll_Func.content, new Vector2(0f, 0f), new Vector2(100f, 100f), new Vector2(0f, 1f), new Vector2(0f, 1f), new Vector2(0f, 1f));
            btn_Item = CreateButton(buttonItemXform, "BtnItem", 28);
            btn_Item.gameObject.SetActive(false);

            RectTransform toggleItemXform = CreateRectTransform("toggle_Item", Scroll_Func.content, new Vector2(0f, 0f), new Vector2(100f, 100f), new Vector2(0f, 1f), new Vector2(0f, 1f), new Vector2(0f, 1f));
            toggle_Item = CreateToggle(toggleItemXform, "ChkItem", 28);
            toggle_Item.gameObject.SetActive(false);

            RectTransform openXform = CreateRectTransform("btn_Open", rectXform,
                new Vector2(canvasScaler.referenceResolution.x / 2 - 50, 0), new Vector2(100f, 100f));
            btn_Open = CreateButton(openXform, "FPS", 36);
            btn_Open.transition = Selectable.Transition.None;

            Dragable dragable = openXform.gameObject.AddComponent<Dragable>();
            dragable.button = btn_Open;

            txt_FPS = openXform.GetComponentInChildren<Text>();

            RectTransform scrollInputXform = CreateRectTransform("Scroll_Input", panelFullXform, new Vector2(0f, -70f), new Vector2(0f, -270f), Vector2.zero, Vector2.one, new Vector2(0.5f, 1f));
            Scroll_Input = CreateScrollRect(scrollInputXform);
            Scroll_Input.horizontal = false;
            Scroll_Input.gameObject.SetActive(false);
            verticalLayoutGroup = Scroll_Input.content.gameObject.AddComponent<VerticalLayoutGroup>();
            verticalLayoutGroup.childForceExpandHeight = false;
            verticalLayoutGroup.childForceExpandWidth = false;
            verticalLayoutGroup.spacing = 20f;
            contentSizeFitter = Scroll_Log.content.gameObject.AddComponent<ContentSizeFitter>();
            contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.MinSize;
            RectTransform txtInputXform = CreateRectTransform("txt_Input", Scroll_Input.content, new Vector2(0f, 0f), new Vector2(Scroll_Input.content.rect.width, 10f), Vector2.up, Vector2.up, new Vector2(0f, 1f));
//             layoutElement = txtInputXform.gameObject.AddComponent<LayoutElement>();
//             layoutElement.minWidth = Scroll_Input.content.rect.width;
//             layoutElement.minHeight = 10f;
            contentSizeFitter = txtInputXform.gameObject.AddComponent<ContentSizeFitter>();
            contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            txt_Input = CreateText(txtInputXform, string.Empty, 28);
            txt_Input.gameObject.SetActive(false);

            RectTransform inputXform = CreateRectTransform("Input_Commond", scrollInputXform, new Vector2(0f, -200f), new Vector2(0f, 200f), Vector2.zero, new Vector2(1f, 0f), new Vector2(0.5f, 0f));
            CreateImage(inputXform, new Color32(81, 177, 255, 255));
            RectTransform inputTextXform = CreateRectTransform("Text", inputXform);
            Text inputText = CreateText(inputTextXform, string.Empty, 36);
            Input_Commond = inputXform.gameObject.AddComponent<InputField>();
            Input_Commond.textComponent = inputText;
            Input_Commond.lineType = InputField.LineType.MultiLineSubmit;
        }

        //private Color gray = new Color32(148, 148, 148, 255);
        //private Color red = new Color32(250, 139, 139, 255);
        private Color green = new Color32(110, 235, 150, 255);
        //private Color bule = new Color32(116, 194, 248, 255);
        private Color yellow = new Color32(249, 202, 104, 255);

        private static Font GetFont() {
            if (m_font != null) {
                return m_font;
            }
            Font[] fonts = Resources.FindObjectsOfTypeAll<Font>();
            if (fonts.Length > 0) {
                m_font = fonts[0];
            }
            return m_font;
        }

        private Sprite m_sprite;
        private Sprite GetSprite() {
            if (m_sprite == null) {
                m_sprite = Resources.Load<Sprite>("Bg_Log");
            }
            return m_sprite;
        }

        private void SetParentAndAlign(GameObject child, RectTransform parent) {
            if (parent == null)
                return;

            child.transform.SetParent(parent, false);
            SetLayerRecursively(child, parent.gameObject.layer);
        }

        private void PaddingParentSize(RectTransform child) {
            child.anchorMin = Vector2.zero;
            child.anchorMax = Vector2.one;
            child.sizeDelta = Vector2.zero;
        }

        private void SetLayerRecursively(GameObject go, int layer) {
            go.layer = layer;
            Transform t = go.transform;
            for (int i = 0; i < t.childCount; i++)
                SetLayerRecursively(t.GetChild(i).gameObject, layer);
        }

        private RectTransform CreateRectTransform(string name, RectTransform parent) {
            GameObject go = new GameObject(name);
            RectTransform rectTransform = go.AddComponent<RectTransform>();
            SetParentAndAlign(go, parent);
            PaddingParentSize(rectTransform);
            //rectTransform.localScale = Vector3.one;
            return rectTransform;
        }

        private RectTransform CreateRectTransform(string name, Transform parent, Vector2 anchoredPosition, Vector2 sizeDelta) {
            GameObject gameObject = new GameObject(name);
            RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
            rectTransform.SetParent(parent);
            rectTransform.anchoredPosition = anchoredPosition;
            rectTransform.sizeDelta = sizeDelta;
            rectTransform.localScale = Vector3.one;
            return rectTransform;
        }

        private RectTransform CreateRectTransform(string name, Transform parent, Vector2 anchoredPosition, Vector2 sizeDelta,
            Vector2 anchorMin, Vector2 anchorMax, Vector2 pivot) {
            GameObject gameObject = new GameObject(name);
            RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
            rectTransform.SetParent(parent);
            rectTransform.anchoredPosition = anchoredPosition;
            rectTransform.sizeDelta = sizeDelta;
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
            rectTransform.pivot = pivot;
            rectTransform.localScale = Vector3.one;
            return rectTransform;
        }

        private Text CreateText(RectTransform xform, string info, int fontSize = 30) {
            GameObject gameObject = xform.gameObject;
            gameObject.AddComponent<CanvasRenderer>();
            Text text = gameObject.AddComponent<Text>();
            text.text = info;
            text.font = GetFont();
            text.fontSize = fontSize;
            text.supportRichText = false;
            text.raycastTarget = false;
            text.lineSpacing = 0.8f;
            return text;
        }

        private Image CreateImage(RectTransform xform, Color color) {
            GameObject gameObject = xform.gameObject;
            gameObject.AddComponent<CanvasRenderer>();
            Image image = gameObject.AddComponent<Image>();
            image.sprite = GetSprite();
            image.color = color;
            return image;
        }

        private Button CreateButton(RectTransform xform, string info, int fontSize = 32) {
            GameObject gameObject = xform.gameObject;
            gameObject.AddComponent<CanvasRenderer>();
            Image image = gameObject.AddComponent<Image>();
            image.sprite = GetSprite();
            Button result = gameObject.AddComponent<Button>();
            RectTransform textXform = CreateRectTransform("Text", xform);
            Text text = CreateText(textXform, info, fontSize);
            text.color = Color.black;
            text.alignment = TextAnchor.MiddleCenter;
            text.resizeTextForBestFit = false;
            text.horizontalOverflow = HorizontalWrapMode.Wrap;
            text.verticalOverflow = VerticalWrapMode.Overflow;
            return result;
        }

        private Toggle CreateToggle(RectTransform xform, string info, int fontSize = 32) {
            GameObject gameObject = xform.gameObject;
            gameObject.AddComponent<CanvasRenderer>();
            Image image = gameObject.AddComponent<Image>();
            image.sprite = GetSprite();
            image.color = yellow;
            RectTransform checkXform = CreateRectTransform("Check", xform);
            Image checkImg = checkXform.gameObject.AddComponent<Image>();
            checkImg.sprite = GetSprite();
            checkImg.color = green;
            Toggle result = gameObject.AddComponent<Toggle>();
            result.graphic = checkImg;
            RectTransform txetXform = CreateRectTransform("Text", xform);
            Text text = CreateText(txetXform, info, fontSize);
            text.color = Color.black;
            text.alignment = TextAnchor.MiddleCenter;
            text.resizeTextForBestFit = false;
            text.horizontalOverflow = HorizontalWrapMode.Wrap;
            text.verticalOverflow = VerticalWrapMode.Overflow;
            return result;
        }

        private ScrollRect CreateScrollRect(RectTransform xform) {
            GameObject gameObject = xform.gameObject;

            RectTransform viewport = CreateRectTransform("Viewport", xform);
            viewport.gameObject.AddComponent<Mask>();
            viewport.anchorMin = Vector2.zero;
            viewport.anchorMax = Vector2.one;
            viewport.sizeDelta = Vector2.zero;
            viewport.pivot = Vector2.up;

            Image viewportImage = viewport.gameObject.AddComponent<Image>();
            viewportImage.sprite = GetSprite();
            viewportImage.color = new Color32(0, 0, 0, 180);

            RectTransform content = CreateRectTransform("Content", viewport);
            content.anchorMin = Vector2.up;
            content.anchorMax = Vector2.one;
            content.sizeDelta = new Vector2(0, 300);
            content.pivot = Vector2.up;

            ScrollRect scrollRect = gameObject.AddComponent<ScrollRect>();
            scrollRect.content = content;
            scrollRect.viewport = viewport;

            return scrollRect;
        }
    }
}