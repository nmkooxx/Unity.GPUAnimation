using Unity.Entities;

/// <summary>
/// 唯一标示
/// </summary>
public struct ActorUID : IComponentData {
    public long value;

    public static implicit operator ActorUID(int e) { return new ActorUID { value = e }; }
    public static implicit operator long(ActorUID e) { return e.value; }
    public static implicit operator ActorUID(long e) { return new ActorUID { value = e }; }
}