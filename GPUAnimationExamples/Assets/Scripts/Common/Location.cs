using Unity.Entities;
using Unity.Mathematics;

/// <summary>
/// 位置，不会有高度变化
/// </summary>
public struct Location : IComponentData {
    /// <summary>
    /// 水平面坐标
    /// </summary>
    public float2 position;

    /// <summary>
    /// 高度
    /// </summary>
    public float height;

    /// <summary>
    /// 角度，弧度
    /// </summary>
    public float angle;

    public float scale;
}