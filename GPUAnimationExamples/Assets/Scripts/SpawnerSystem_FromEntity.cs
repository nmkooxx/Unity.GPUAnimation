using System.Diagnostics;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Analytics;
using Debug = UnityEngine.Debug;

// JobComponentSystems can run on worker threads.
// However, creating and removing Entities can only be done on the main thread to prevent race conditions.
// The system uses an EntityCommandBuffer to defer tasks that can't be done inside the Job.

// ReSharper disable once InconsistentNaming
[UpdateInGroup(typeof(SimulationSystemGroup))]
public class SpawnerSystem_FromEntity : JobComponentSystem
{
    // BeginInitializationEntityCommandBufferSystem is used to create a command buffer which will then be played back
    // when that barrier system executes.
    // Though the instantiation command is recorded in the SpawnJob, it's not actually processed (or "played back")
    // until the corresponding EntityCommandBufferSystem is updated. To ensure that the transform system has a chance
    // to run on the newly-spawned entities before they're rendered for the first time, the SpawnerSystem_FromEntity
    // will use the BeginSimulationEntityCommandBufferSystem to play back its commands. This introduces a one-frame lag
    // between recording the commands and instantiating the entities, but in practice this is usually not noticeable.
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    public static void SetParent(EntityCommandBuffer.Concurrent dstManager, int jobIndex, Entity parent, Entity child, float3 localTranslation) {
        //set the child
        dstManager.SetComponent(jobIndex, child, new Translation { Value = localTranslation });
        dstManager.AddComponent(jobIndex, child, new Parent { Value = parent });
        dstManager.AddComponent(jobIndex, child, new LocalToParent());

        //set the parent
//         if (!dstManager.HasComponent<LocalToWorld>(parent))
//             dstManager.AddComponent(jobIndex, parent, new LocalToWorld { });
//
//         if (!dstManager.HasComponent<Translation>(parent))
//             dstManager.AddComponent(jobIndex, parent, new Translation { Value = Vector3.one });
//
//         if (!dstManager.HasComponent<Rotation>(parent))
//             dstManager.AddComponent(jobIndex, parent, new Rotation { Value = Quaternion.identity });
//
//         if (!dstManager.HasComponent<NonUniformScale>(parent))
//             dstManager.AddComponent(jobIndex, parent, new NonUniformScale { Value = Vector3.one });

    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        //Instead of performing structural changes directly, a Job can add a command to an EntityCommandBuffer to perform such changes on the main thread after the Job has finished.
        //Command buffers allow you to perform any, potentially costly, calculations on a worker thread, while queuing up the actual insertions and deletions for later.

        // Schedule the job that will add Instantiate commands to the EntityCommandBuffer.
        var CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent();
        var job = Entities
            //.WithoutBurst()
            .ForEach((Entity entity, int entityInQueryIndex, in Spawner_FromEntity spawnerFromEntity, in DynamicBuffer<TestData> dynamicBuffer) => {
                var Dist = spawnerFromEntity.Dist;
                var OffsetX = spawnerFromEntity.OffsetX;
                var OffsetY = spawnerFromEntity.OffsetY;
                for (var x = 0; x < spawnerFromEntity.CountX; x++) {
                    for (var y = 0; y < spawnerFromEntity.CountY; y++) {
                        var instance = CommandBuffer.Instantiate(entityInQueryIndex, spawnerFromEntity.Prefab);

                        // Place the instantiated in a grid with some noise
                        //var position = math.transform(location.Value, new float3(x * 0.7F, noise.cnoise(new float2(x, y) * 0.21F) * 2, y * 0.7F));
                        //var position = new float3(OffsetX + x * Dist, 0, OffsetY + y * Dist);
//                         CommandBuffer.SetComponent(entityInQueryIndex, instance, new Translation { Value = position });
//                         CommandBuffer.AddComponent(entityInQueryIndex, instance, new Parent { Value = entity });
                        var position = new float3(x * Dist.x, 0, y * Dist.y);
                        SetParent(CommandBuffer, entityInQueryIndex, entity, instance, position);

                        CommandBuffer.AppendToBuffer(entityInQueryIndex, entity, new TestData { anim = instance });
                    }
                }
                CommandBuffer.RemoveComponent<Spawner_FromEntity>(entityInQueryIndex, entity);

                if (spawnerFromEntity.Fx != Entity.Null) {
                    var fx = CommandBuffer.Instantiate(entityInQueryIndex, spawnerFromEntity.Fx);
                    SetParent(CommandBuffer, entityInQueryIndex, entity, fx, new float3());

                    //var fxPrefab = Test.m_fxs[0];

                }

                //new GameObject("ssss");
                //Test.Pop();

                //CommandBuffer.DestroyEntity(entityInQueryIndex, entity);
            }).Schedule(inputDeps);


        // SpawnJob runs in parallel with no sync point until the barrier system executes.
        // When the barrier system executes we want to complete the SpawnJob and then play back the commands (Creating the entities and placing them).
        // We need to tell the barrier system which job it needs to complete before it can play back the commands.
        m_EntityCommandBufferSystem.AddJobHandleForProducer(job);

        return job;
    }
}
