using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

// ReSharper disable once InconsistentNaming
public struct Spawner_FromEntity : IComponentData
{
    public int index;
    public int OffsetX;
    public int OffsetY;
    public int CountX;
    public int CountY;
    public float2 Dist;
    public Entity Prefab;

    public Entity Fx;

    //     public int changed;
    //     public NativeList<Entity> list;
    //     public SimpleAnim anim;
}
