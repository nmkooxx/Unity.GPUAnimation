using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

//[DisableAutoCreation]
[UpdateInGroup(typeof(TransformSystemGroup))]
//[UpdateBefore(typeof(BeginInitializationEntityCommandBufferSystem))]
public class SailingSystem : JobComponentSystem {

    protected override JobHandle OnUpdate(JobHandle inputDeps) {
        //inputDeps.Complete();

        int time = (int)Time.ElapsedTime;
        var handle = new SailingJob() {
            time = time
        }.Schedule(this, inputDeps);
        //handle.Complete();

        return handle;
    }

    private const float kDoublePI = math.PI * 2;
    private const float kHalfPI = math.PI * 0.5f;

    [BurstCompile] // Disbaled burst, as Debug.Log is used.
    struct SailingJob : IJobForEach_BCC<SailingData, Location, LocalToWorld> {
        public int time;

        private int passTime;
        // Allow buffer read write in parralel jobs
        // Ensure, no two jobs can write to same entity, at the same time.
        public void Execute(DynamicBuffer<SailingData> dynamicBuffer, ref Location location, ref LocalToWorld ltd) {
            if (dynamicBuffer.Length <= 0) {
                //UnityEngine.Debug.Log("skip Sailing:" + location.position);
                return;
            }
            SailingData path = dynamicBuffer[0];
            if (!path.init) {
                path = init(dynamicBuffer, location, 0);
            }
            passTime = time - path.startTime;

            bool finish = false;
            switch (path.type) {
                case eSailingType.Line:
                    finish = updatePathLine(path, ref location);
                    break;
                case eSailingType.Turn:
                    finish = updatePathTurn(path, ref location);
                    break;
                case eSailingType.Round:
                    finish = updatePathRound(path, ref location);
                    break;
                default:
                    //Debug.LogError("UpdatePath error type:" + path.type);
                    break;
            }

            ltd.Value = float4x4.TRS(
                new float3(location.position.x, location.height, location.position.y),
                quaternion.Euler(0, location.angle, 0),
                new float3(location.scale, location.scale, location.scale)
                );

            if (!finish) {
                return;
            }
            dynamicBuffer.RemoveAt(0);
            if (dynamicBuffer.Length <= 0) {
                //TODO 看能不能删掉这个Buffer
                return;
            }

            int offsetTime = path.dura - passTime;
            init(dynamicBuffer, location, offsetTime);
        }

        private float repeat(float value, float length) {
            int num = (int)((value) / length);
            float result = math.select(
                value - num * length,
                value + (1 - num) * length,
                value < 0);
            return result;
        }

        private SailingData init(DynamicBuffer<SailingData> dynamicBuffer, Location location, int offsetTime) {
            SailingData path = dynamicBuffer[0];
            path.init = true;
            path.startTime = time + offsetTime;
            switch (path.type) {
                case eSailingType.Line:
                    path.start = location.position;
                    break;
                case eSailingType.Turn:
                    path.startAngle = location.angle;
                    path.angle = repeat(path.angle, kDoublePI);
                    path.startAngle = repeat(path.startAngle, kDoublePI);
                    break;
                case eSailingType.Round:
                    path.start = location.position;
                    path.startAngle = location.angle;
                    float leftAngle = path.startAngle - kHalfPI;
                    float2 left = new float2(math.sin(leftAngle), math.cos(leftAngle));
                    float2 vec = new float2(path.center.x - path.start.x, path.center.y - path.start.y);
                    //判断旋转中心是否前进方向的左边的同方向
                    float dot = math.dot(left, vec);
                    path.clockwise = dot < 0;
                    path.angle = repeat(path.angle, kDoublePI);
                    path.startAngle = repeat(path.startAngle, kDoublePI);
                    path.startAngle = math.select(
                        math.select(path.startAngle, path.startAngle + kDoublePI, path.startAngle < path.angle),
                        math.select(path.startAngle, path.startAngle - kDoublePI, path.startAngle > path.angle),
                        path.clockwise);
                    //Debug.LogError("init:" + path.start + path.dest + " angle:" + math.degrees(path.startAngle) + "->" + math.degrees(path.angle) + " left:" + math.degrees(leftAngle) + " " + math.degrees(location.angle) + " dot:" + dot + " clockwise:" + path.clockwise);
                    break;
                default:
                    //Debug.LogError("init error type:" + path.type);
                    break;
            }
            dynamicBuffer[0] = path;
            return path;
        }

        private bool updatePathLine(SailingData path, ref Location location) {
            bool finish = passTime >= path.dura;
            var pos = math.lerp(path.start, path.dest, (float)passTime / path.dura);
            location.position = math.select(pos, path.dest, finish);
            return finish;
        }

        private bool updatePathTurn(SailingData path, ref Location location) {
            bool finish = passTime >= path.dura;
            float angle = math.lerp(path.startAngle, path.angle, (float)passTime / path.dura);
            location.angle = math.select(angle, path.angle, finish);
            return finish;
        }

        private bool updatePathRound(SailingData path, ref Location location) {
            bool finish = passTime >= path.dura;
            float per = (float)passTime / path.dura;
            float angle = math.lerp(path.startAngle, path.angle, per);
            location.angle = math.select(angle, path.angle, finish);

            float rad = math.select(angle + kHalfPI, angle - kHalfPI, path.clockwise);
            float2 pos = path.center;
            pos.x += math.sin(rad) * path.radius;
            pos.y += math.cos(rad) * path.radius;
            location.position = math.select(pos, path.dest, finish);

            //Debug.Log("pos:" + pos + " per:" + per + " angle:" + math.degrees(angle));
            return finish;
        }
    }
}

