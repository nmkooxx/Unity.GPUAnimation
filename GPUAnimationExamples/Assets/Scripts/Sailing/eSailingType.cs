public enum eSailingType {
    /// <summary>
    /// 直线运动
    /// </summary>
    Line,
    /// <summary>
    /// 原地转向
    /// </summary>
    Turn,
    /// <summary>
    /// 拐弯
    /// </summary>
    Round
}