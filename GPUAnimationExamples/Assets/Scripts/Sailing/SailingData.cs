using System;
using Unity.Entities;
using Unity.Mathematics;

/// <summary>
/// 航行数据
/// </summary>
public struct SailingData : IBufferElementData {
    public bool init;
    public eSailingType type;
    public int startTime;
    public int dura;
    public float2 start;
    public float2 dest;
    public float2 center;
    public float radius;
    public float startAngle;
    public float angle;
    public bool clockwise;

    public override string ToString() {
        string serialize = string.Format("< type:{0} angle:{1} dest:{2} dura:{3}>", type, angle, dest, dura);
        return serialize;
    }
}