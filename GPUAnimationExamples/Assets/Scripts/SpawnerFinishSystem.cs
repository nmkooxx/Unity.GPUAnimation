// using System.Diagnostics;
// using Unity.Collections;
// using Unity.Entities;
// using Unity.Jobs;
// using Unity.Mathematics;
// using Unity.Transforms;
// using UnityEngine.Analytics;
// using Debug = UnityEngine.Debug;
//
// // JobComponentSystems can run on worker threads.
// // However, creating and removing Entities can only be done on the main thread to prevent race conditions.
// // The system uses an EntityCommandBuffer to defer tasks that can't be done inside the Job.
//
// // ReSharper disable once InconsistentNaming
// [UpdateInGroup(typeof(SimulationSystemGroup))]
// [UpdateAfter(typeof(SpawnerSystem_FromEntity))]
// public class SpawnerFinishSystem : JobComponentSystem
// {
//     // BeginInitializationEntityCommandBufferSystem is used to create a command buffer which will then be played back
//     // when that barrier system executes.
//     // Though the instantiation command is recorded in the SpawnJob, it's not actually processed (or "played back")
//     // until the corresponding EntityCommandBufferSystem is updated. To ensure that the transform system has a chance
//     // to run on the newly-spawned entities before they're rendered for the first time, the SpawnerSystem_FromEntity
//     // will use the BeginSimulationEntityCommandBufferSystem to play back its commands. This introduces a one-frame lag
//     // between recording the commands and instantiating the entities, but in practice this is usually not noticeable.
//     BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;
//
//     protected override void OnCreate()
//     {
//         // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
//         m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
//     }
//
//     protected override JobHandle OnUpdate(JobHandle inputDeps)
//     {
//         //Instead of performing structural changes directly, a Job can add a command to an EntityCommandBuffer to perform such changes on the main thread after the Job has finished.
//         //Command buffers allow you to perform any, potentially costly, calculations on a worker thread, while queuing up the actual insertions and deletions for later.
//
//         if (!inputDeps.IsCompleted) {
//             return default;
//         }
//         //inputDeps.Complete();
//
//         var manager = World.DefaultGameObjectInjectionWorld.EntityManager;
//         var CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent();
//         // Schedule the job that will add Instantiate commands to the EntityCommandBuffer.
//         var job = Entities
//             .WithoutBurst()
//             .ForEach((Entity entity, int entityInQueryIndex, ref OwnIndexShared owner) => {
//                 if (!owner.init) {
//                     owner.init = true;
//                     Test.m_entities[owner.index].Add(entity);
//                 }
//                 //Test.m_entities[owner.index].Add(entity);
//                 //manager.RemoveComponent<OwnIndexShared>(entity);
//                 //CommandBuffer.RemoveComponent<OwnIndexShared>(entityInQueryIndex, entity);
//             }).Schedule(inputDeps);
//
//         job.Complete();
//         return job;
//     }
// }
