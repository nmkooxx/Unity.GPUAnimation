using System;
using Unity.Entities;
using Unity.Mathematics;

/// <summary>
/// 巡逻数据
/// </summary>
public struct PatrolData : IBufferElementData {
    public float startTime;
    public float dura;
    public float2 start;
    public float2 dest;
}