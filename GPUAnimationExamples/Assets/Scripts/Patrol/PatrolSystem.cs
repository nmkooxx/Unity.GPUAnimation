using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

//[DisableAutoCreation]
[UpdateInGroup(typeof(TransformSystemGroup))]
//[UpdateBefore(typeof(BeginInitializationEntityCommandBufferSystem))]
public class PatrolSystem : JobComponentSystem {

    protected override JobHandle OnUpdate(JobHandle inputDeps) {
        //inputDeps.Complete();

        float time = (float)Time.ElapsedTime;
        var handle = new SailingJob() {
            time = time
        }.Schedule(this, inputDeps);
        //handle.Complete();

        return handle;
    }

    [BurstCompile] // Disbaled burst, as Debug.Log is used.
    struct SailingJob : IJobForEach_BCC<PatrolData, Location, LocalToWorld> {
        public float time;

        private float passTime;
        // Allow buffer read write in parralel jobs
        // Ensure, no two jobs can write to same entity, at the same time.
        public void Execute(DynamicBuffer<PatrolData> dynamicBuffer, ref Location location, ref LocalToWorld ltd) {
            if (dynamicBuffer.Length <= 0) {
                //UnityEngine.Debug.Log("skip Sailing:" + location.position);
                return;
            }
            PatrolData path = dynamicBuffer[0];
            passTime = time - path.startTime;

            bool finish = passTime >= path.dura;
            var pos = math.lerp(path.start, path.dest, (float)passTime / path.dura);
            location.position = math.select(pos, path.dest, finish);

            ltd.Value = float4x4.TRS(
                new float3(location.position.x, location.height, location.position.y),
                quaternion.Euler(0, location.angle, 0),
                new float3(location.scale, location.scale, location.scale)
                );

            if (!finish) {
                return;
            }
            dynamicBuffer.RemoveAt(0);
            if (dynamicBuffer.Length <= 0) {
                //TODO 看能不能删掉这个Buffer
                return;
            }
        }
    }
}

