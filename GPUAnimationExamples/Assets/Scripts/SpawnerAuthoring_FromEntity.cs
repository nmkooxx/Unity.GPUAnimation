using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

// ReSharper disable once InconsistentNaming
[RequiresEntityConversion]
public class SpawnerAuthoring_FromEntity : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
{
    public GameObject[] Prefabs;
    public int CountX;
    public int CountY;
    public float Dist;

    // Referenced prefabs have to be declared so that the conversion system knows about them ahead of time
    public void DeclareReferencedPrefabs(List<GameObject> gameObjects)
    {
        gameObjects.AddRange(Prefabs);
    }

    // Lets you convert the editor data representation to the entity optimal runtime representation

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        int OffsetX = (int)transform.position.x;
        int OffsetY = (int)transform.position.z;
        foreach (var Prefab in Prefabs) {
            var spawnerData = new Spawner_FromEntity
            {
                // The referenced prefab will be converted due to DeclareReferencedPrefabs.
                // So here we simply map the game object to an entity reference to that prefab.
                Prefab = conversionSystem.GetPrimaryEntity(Prefab),
                OffsetX = OffsetX,
                OffsetY = OffsetY,
                CountX = CountX,
                CountY = CountY,
                Dist = Dist,
            };
            //Debug.Log("Prefab:" + Prefab);
            dstManager.AddComponentData(entity, spawnerData);
            OffsetX += 35;
            break;
        }
    }
}
