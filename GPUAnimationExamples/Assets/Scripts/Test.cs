using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Core;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public struct OwnIndexShared : IComponentData {
    public int index;
    public bool init;
}

public struct TestData : IBufferElementData {
    public Entity anim;
}

public struct ChangeAnimData : IComponentData {
    public int animIndex;

}

public class Test : MonoBehaviour
{
    public GameObject[] Prefabs = new GameObject[1];
    public GameObject[] Fxs = new GameObject[1];
    public Text text;

    private int m_select;
    private int m_selectFx;
    EntityManager m_manager;
    Entity[] m_prefabs;
    public static Entity[] m_fxs;
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;
    Camera m_camera;
    LayerMask m_clickMask;

    private static Queue<GameObject> m_gos = new Queue<GameObject>(1000);
    public static GameObject Pop() {
        var go = m_gos.Dequeue();
        //go.SetActive(true);
        return go;
    }

    void Start()
    {
        ULog.Create(text.font).Show();
        Physics.autoSimulation = false;
        Physics.autoSyncTransforms = false;

        m_camera = Camera.main;
        m_clickMask = LayerMask.GetMask("Water");

        m_manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var setting = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        m_prefabs = new Entity[Prefabs.Length];
        for (int i = 0; i < Prefabs.Length; i++) {
            var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(Prefabs[i], setting);
            m_prefabs[i] = entity;
            //Debug.Log(i + " Prefab:" + entity);
        }

        setting.FilterFlags = WorldSystemFilterFlags.HybridGameObjectConversion;
        //setting.BlobAssetStore = BlobAssetStore;
        m_fxs = new Entity[Fxs.Length];
        for (int i = 0; i < Fxs.Length; i++) {
            var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(Fxs[i], setting);
            m_fxs[i] = entity;
            //Debug.Log(i + " Fx:" + entity);
        }

        for (int i = 0; i < 10; i++) {
            var go = new GameObject();
            //go.SetActive(false);
            m_gos.Enqueue(go);
        }

        m_EntityCommandBufferSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();

        m_manager.Instantiate(m_fxs[0]);
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject != null) {
            return;
        }

        if (Input.GetMouseButtonUp(0)) {
            Spawn();
        }

        if (Input.GetMouseButtonUp(1)) {
            SpawnFx();
        }
    }

    public void PrevSelect() {
        --m_select;
        if (m_select < 0) {
            m_select = Prefabs.Length - 1;
        }
        text.text = Prefabs[m_select].name;
    }

    public void NextSelect() {
        ++m_select;
        if (m_select >= Prefabs.Length) {
            m_select = 0;
        }
        text.text = Prefabs[m_select].name;
    }

    public struct SpwanJob : IJob {
        public float a;
        public float b;
        public NativeArray<float> result;

        public void Execute() {
            result[0] = a + b;
        }
    }

    public int CountX = 3;
    public int CountY = 4;
    public Vector2 dist = new Vector2(1, 2);
    Spawner_FromEntity spawnerData;
    Entity entity;

    private void Spawn() {
        Ray ray = m_camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, 500, m_clickMask.value)) {
            Debug.LogError("Raycast miss:" + ray.origin);//碰撞坐标
            return;
        }
        Debug.Log("Raycast hit:" + hit.point);

        ++m_selectFx;
        if (m_selectFx >= Fxs.Length) {
            m_selectFx = 0;
        }

        var pos = hit.point;
        spawnerData = new Spawner_FromEntity {
            // The referenced prefab will be converted due to DeclareReferencedPrefabs.
            // So here we simply map the game object to an entity reference to that prefab.
            Prefab = m_prefabs[m_select],
            Fx = m_fxs[m_selectFx],
            OffsetX = (int)pos.x,
            OffsetY = (int)pos.z,
            CountX = CountX,
            CountY = CountY,
            Dist = dist,
            index = m_select,

//             changed = 1,
//             list = new NativeList<Entity>(1000, Allocator.Persistent),
        };

        entity = m_manager.CreateEntity();
        m_manager.SetName(entity, "Spawn_" + (int)pos.x + "_" + (int)pos.z);
        //m_entities[m_select].Add(entity);
        m_manager.AddBuffer<TestData>(entity);

        Location location = new Location() {
            position = new float2(pos.x, pos.z),
            angle = 0,
            height = pos.y,
            scale = 1
        };
        m_manager.AddComponentData(entity, location);
        m_manager.AddComponentData(entity, new LocalToWorld {
            Value = float4x4.TRS(
                    new float3(location.position.x, location.height, location.position.y),
                    quaternion.Euler(0, location.angle, 0),
                    new float3(location.scale, location.scale, location.scale)
                    )
        });


        var patrolBuffer = m_manager.AddBuffer<PatrolData>(entity);
        var start = new float2(pos.x, pos.z);
        var dest = new float2(start.x + 50, start.y + 10);
        var startTime = UnityEngine.Time.time;
        var dura = 10;
        for (int i = 0; i < 10; i++) {
            patrolBuffer.Add(new PatrolData { start = start, dest = dest, startTime = startTime, dura = dura });
            startTime += dura;
            patrolBuffer.Add(new PatrolData { start = dest, dest = start, startTime = startTime, dura = dura });
            startTime += dura;
        }

        GameObject go = new GameObject();
        GameObject fx = GameObject.Instantiate(Fxs[m_selectFx], new Vector3(0, 0.1f), Quaternion.identity, go.transform);
        m_manager.AddComponentObject(entity, go.transform);
        m_manager.AddComponentData(entity, new CopyTransformToGameObject());
        //         var setting = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        //         var fxEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(fx, setting);

        m_manager.AddComponentData(entity, spawnerData);
    }

    private int animIndex = 0;
    public void PlayAnim() {
        ++animIndex;
        if (animIndex >= 3) {
            animIndex = 0;
        }
        var changeAnimData = new ChangeAnimData { animIndex = animIndex };
        m_manager.AddComponentData(entity, changeAnimData);
    }

    private void SpawnFx() {
        Ray ray = m_camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, 500, m_clickMask.value)) {
            Debug.LogError("Raycast miss:" + ray.origin);//碰撞坐标
            return;
        }
        Debug.Log("SpawnFx Raycast hit:" + hit.point);

        ++m_selectFx;
        if (m_selectFx >= Fxs.Length) {
            m_selectFx = 0;
        }

        var entity = m_manager.Instantiate(m_fxs[m_selectFx]);

    }
}
