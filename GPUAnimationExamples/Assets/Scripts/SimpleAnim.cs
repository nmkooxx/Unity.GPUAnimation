using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;
using Unity.GPUAnimation;
using Unity.Mathematics;

public struct SimpleAnim : IComponentData
{
    public int  ClipIndex;
    public float Speed;
    public bool IsFirstFrame;
    public bool RandomizeStartTime;
    public float2 RandomizeMinMaxSpeed;
}

[UpdateInGroup(typeof(PresentationSystemGroup))]
[UpdateBefore(typeof(CalculateTextureCoordinateSystem))]
public class SimpleAnimSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var DeltaTime = Time.DeltaTime;
        var job = Entities
            .WithBurst()
            .ForEach((Entity entity, int entityInQueryIndex, ref SimpleAnim simple, ref GPUAnimationState animstate) => {
            animstate.AnimationClipIndex = simple.ClipIndex;

            ref var clips = ref animstate.AnimationClipSet.Value.Clips;
            if ((uint)animstate.AnimationClipIndex < (uint)clips.Length) {
                if (!simple.IsFirstFrame) {
                    animstate.Time += DeltaTime * simple.Speed;
                }
                else {
                    var length = 10.0F;

                    var random = new Unity.Mathematics.Random((uint)entityInQueryIndex + 1);

                    // For more variety randomize state more...
                    random.NextInt();

                    if (simple.RandomizeStartTime)
                        animstate.Time = random.NextFloat(0, length);
                    else
                        animstate.Time = 0;
                    simple.Speed = random.NextFloat(simple.RandomizeMinMaxSpeed.x, simple.RandomizeMinMaxSpeed.y);

                    simple.IsFirstFrame = false;
                }
            }
            else {
                // @TODO: Warnings?
            }
        }).Schedule(inputDeps);
        return job;
    }
}